#include <functional>
#include <iostream>
#include <numeric>
#include <vector>
#include <cstdlib>
#include <sstream>
#include <exception>
#include <optional>
#include <istream>

template <class U>
struct Lecture
{
    std::optional<U> value{}; // je suis pas sur, est ce que c'est pas plutot un parser d'optional qu'il me faut
    std::string      reste{};
};

Lecture<char> splitFirstChar(std::string str)
{
    if (str.empty()) return {};

    Lecture<char> result{};

    result.value = str[0];
    result.reste = std::string(str.begin() + 1, str.end());

    return result;
}

template <class T>
class Parser
{
public:
private:
    Parser() = default;

    // je dois donner le nouveau string au suivant donc pas le choix je dois retourner 2 trucs
    std::function<Lecture<T>(std::string)> reader;

    bool              consume_nothing{};
    std::vector<char> start_char{};

public:
    Lecture<T> read(std::string text) const { return reader(text); }

    friend Parser<char> charP();

    friend Parser<char> charP(char c);

    // very sus
    template <class A>
    friend Parser<A> either(Parser<A> first, Parser<A> second);

    template <class V>
    friend Parser<V> pure(V value);

    template <class Function, class V>
    friend Parser<std::result_of_t<Function(V)>> apply(Parser<Function> func_parser, Parser<V> value_parser);

    template <class A>
    friend Parser<std::vector<A>> zeroOrMore(Parser<A> a);

    template <class A>
    friend Parser<A> drop(Parser<A> parser);
};

// ? on fait quoi si on parse un truc vide ?

Parser<char> charP()
{
    Parser<char> result{};

    result.consume_nothing = false;

    // tres moche, une function serait plus dans le theme ?
    size_t nbChar = 256;
    result.start_char.resize(nbChar);
    std::iota(result.start_char.begin(), result.start_char.end(), (char)0);

    // comment je passe l'info que j'ai manger qqch
    result.reader = [](auto str) { return splitFirstChar(str); };

    return result;
}

Parser<char> charP(char c)
{
    Parser<char> result{};

    result.consume_nothing = false;

    result.start_char.push_back(c);

    result.reader = [c](auto str) {
        if (str[0] != c)
        {
            std::stringstream ss;
            ss << "expected : " << c << " got =" << str[0] << ".";
            throw std::invalid_argument(ss.str());
        }
        return splitFirstChar(str);
    };

    return result;
}

// very sus
template <class A>
Parser<A> either(Parser<A> first, Parser<A> second)
{
    Parser<A> result{};

    result.consume_nothing = first.consume_nothing; // ?.????
    result.start_char      = [&]() {
        auto all = first.start_char;
        all.insert(all.end(), second.start_char.begin(), second.start_char.end());
        return all;
    }();

    result.reader = [first, second](auto str) {
        const char c = str[0];
        if (std::find(first.start_char.begin(), first.start_char.end(), c) != first.start_char.end())
        {
            return first.read(str);
        }
        else
        {
            return second.read(str);
        }
    };

    return result;
}

template <class V>
Parser<V> pure(V value)
{
    Parser<V> result{};
    result.consume_nothing = true;
    result.reader          = [value](auto str) {
        Lecture<V> result;
        result.reste = str;
        result.value = value;
        return result;
    };
    return result;
}

template <class Function, class V>
Parser<std::result_of_t<Function(V)>> apply(Parser<Function> func_parser, Parser<V> value_parser)
{
    using return_t = std::result_of_t<Function(V)>;
    Parser<return_t> result{};

    result.consume_nothing = func_parser.consume_nothing && value_parser.consume_nothing;

    // il se passe quoi si les deux consume qqch ?
    if (func_parser.consume_nothing)
        result.start_char = value_parser.start_char;
    else
        result.start_char = func_parser.start_char;

    result.reader = [func_parser, value_parser](auto str) {
        auto [function, funOutput] = func_parser.read(str);

        if (not function)
        {
            std::stringstream ss;
            ss << "pas de function :/.";
            throw std::invalid_argument(ss.str());
        }

        auto [value, reste] = value_parser.read(str);

        Lecture<return_t> result;
        result.reste = reste;

        if (not value) return result;

        result.value = function.value()(value.value());

        return result;
    };

    return result;
}

auto char_to_digit = pure([](char c) { return c - '0'; });
auto digits_to_int = pure([](std::vector<int> digits) {
    int value = 0;
    for (const auto d : digits)
    {
        value = value * 10 + d;
    }
    return value;
});

Parser<int> digitP = apply(char_to_digit, either(charP('0'),                                                         //
                                                 either(charP('1'),                                                  //
                                                        either(charP('2'),                                           //
                                                               either(charP('3'),                                    //
                                                                      either(charP('4'),                             //
                                                                             either(charP('5'),                      //
                                                                                    either(charP('6'),               //
                                                                                           either(charP('7'),        //
                                                                                                  either(charP('8'), //
                                                                                                         charP('9')  //
                                                                                                         ))))))))));

template <class A>
Parser<std::vector<A>> zeroOrMore(Parser<A> value_parseur)
{
    Parser<std::vector<A>> result{};

    result.reader = [value_parseur](auto str) {
        char c = str[0];

        Lecture<std::vector<A>> lecture{};
        lecture.reste = str;
        int indice    = 1;
        while (std::find(value_parseur.start_char.begin(), value_parseur.start_char.end(), c) !=
               value_parseur.start_char.end())
        {
            auto [value, reste] = value_parseur.read(lecture.reste);
            if (value)
            {
                if (not lecture.value)
                {
                    lecture.value = std::vector<A>{};
                }
                lecture.value.value().push_back(value.value());
            }

            lecture.reste = reste;

            c = str[indice];
            indice++;
        }

        return lecture;
    };

    result.consume_nothing = false;

    return result;
}

// est ce que j'ai moyen d'ecrire ça avec l'interface que j'ai sans friend
// je pense pas mais j'en ai pas forcement l'usage, je peux juste lire et ignorer
template <class A>
Parser<A> drop(Parser<A> parser)
{
    Parser<A> result{};
    result.consume_nothing = false;
    result.reader          = [parser](auto str) {
        Lecture<A> result;
        result.reste = parser.read(str).reste;
        result.value = {};
        return result;
    };
    return result;
}

template <class T>
std::optional<T> ignore(const T&)
{
    return {};
};

// // comme drop mais pas amie, marche pas
// template <class A>
// Parser<A> skip(Parser<A> parser)
// {
//     // auto ignoreLbd      = pure(ignore<A>);
//     // auto optionalParser = apply(pure([](auto v) { return std::optional{v}; }), parser);
//     // auto aParser = apply(pure([](auto v) { return std::optional{v}; }), optionalParser);

//     return pure(A{});
// }

int main()
{
    std::cout << "start\n";

    std::string monText = "azer 43 [qdsf]";

    auto skip1 = charP();

    auto step1 = skip1.read(monText);
    std::cout << step1.reste << std::endl;
    std::cout << charP('z').read(step1.reste).value.value() << std::endl;
    std::cout << charP('z').read(step1.reste).reste << std::endl;

    std::string mesChiffres = "156165 43 1";
    std::cout << digitP.read(mesChiffres).value.value() << std::endl;

    Parser<int> intP = apply(digits_to_int, zeroOrMore(digitP));
    std::cout << intP.read(mesChiffres).value.value() << std::endl;
    {
        Parser<int> dropIntP = drop(intP);
        std::cout << dropIntP.read(mesChiffres).value.has_value() << std::endl;
        std::cout << dropIntP.read(mesChiffres).reste << std::endl;
    }
    {
        Parser<int> dropIntP = skip(intP);
        std::cout << dropIntP.read(mesChiffres).value.has_value() << std::endl;
        std::cout << dropIntP.read(mesChiffres).reste << std::endl;
    }
}

// // lit trop tot le bousin
// // template <class A, class B>
// // Parser<std::pair<A, B>> operator>>(Parser<A> first_parseur, Parser<B> second_parser)
// // {
// //     return pure(std::pair<A, B>(first_parseur.read(), second_parser.read()));
// // }

// template <class A, class B>
// Parser<std::pair<A, B>> operator>>(Parser<A> first_parseur, Parser<B> second_parser)
// {
//     return apply(pure([=](auto first) mutable {
//                      return std::make_pair(first, second_parser.read());
//                  }), // doit retourn std::pair<A, B>
//                  first_parseur);
// }
