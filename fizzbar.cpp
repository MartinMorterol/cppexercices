#include <ranges>
#include <iostream>
#include <algorithm>

int main()
{
	namespace sr = std::ranges;
	namespace sv = std::views;

	constexpr int  max  = 16;
	constexpr auto fizz = std::pair(3, "fizz");
	constexpr auto buzz = std::pair(5, "bar");

	struct intStr
	{
		int         i{};
		std::string str{};
	};

	auto toIntStr = [](auto i) { return intStr{ i, "" }; };

	auto createActor = [](auto description) {
		return [description](auto intStr) {
			if (intStr.i % description.first == 0)
			{
				intStr.str += std::string(description.second);
			}
			return intStr;
		};
	};

	auto toStr = [](auto intStr) {
		if (intStr.str.empty())
		{
			return std::to_string(intStr.i);
		}
		return intStr.str;
	};

	auto generator = sv::iota(0) | sv::transform(toIntStr) | sv::transform(createActor(fizz))
	                 | sv::transform(createActor(buzz)) | sv::transform(toStr);

	for (auto i : generator | sv::take(max))
		std::cout << i << ' ';
	std::cout << '\n';
}