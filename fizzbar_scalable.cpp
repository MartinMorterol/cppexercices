#include <ranges>
#include <iostream>
#include <array>
#include <tuple>
#include <algorithm>

template <class Array, size_t idx = 0>
constexpr auto createTransformer(const Array& arr)
{
	namespace sv = std::views;

	constexpr auto size = std::tuple_size<Array>::value;

	if constexpr (size == idx)
	{
		constexpr auto identity = [](auto a) constexpr { return a; };
		return sv::transform(identity);
	}
	else
	{
		constexpr auto createActor = [](auto description) constexpr
		{
			return [description](auto intStr) constexpr
			{
				if (intStr.i % description.first == 0)
				{
					intStr.str += std::string(description.second);
				}
				return intStr;
			};
		};

		auto elem = arr[idx];
		return sv::transform(createActor(elem)) | createTransformer<Array, idx + 1>(arr);
	}
}

int main()
{
	namespace sr = std::ranges;
	namespace sv = std::views;

	constexpr int  max            = 16;
	constexpr auto transformDescr = std::array{ std::pair(3, "fizz"), std::pair(5, "bar") };

	struct intStr
	{
		int         i{};
		std::string str{};
	};

	constexpr auto toIntStr = [](auto i) constexpr { return intStr{ i, "" }; };

	constexpr auto toStr = [](auto intStr) constexpr
	{
		if (intStr.str.empty())
		{
			return std::to_string(intStr.i);
		}
		return intStr.str;
	};

	constexpr auto generator =
	    sv::iota(0) | sv::transform(toIntStr) | createTransformer(transformDescr) | sv::transform(toStr);

	std::ranges::for_each(
	    generator | sv::take(max), [](const auto& word) constexpr { std::cout << word << ' '; });
	std::cout << '\n';
}